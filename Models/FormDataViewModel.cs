using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DSIELABGRMVCTest.Models.Enums;
using DSIELABGRMVCTest.Models.Validations;

namespace DSIELABGRMVCTest.Models
{
    public class FormDataViewModel
    {
        [Required(ErrorMessage="Este campo es obligatorio.")]
        [StringLength(60, ErrorMessage = "La longitud del campo {0} debe ser entre {1} y {2}.", MinimumLength = 3)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = "El valor introducido es incorrecto.")]
        public string Name { get; set; }

        [Required(ErrorMessage="Este campo es obligatorio")]
        [StringLength(60, ErrorMessage = "La longitud del campo {0} debe ser entre {1} y {2}.", MinimumLength = 3)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$", ErrorMessage = "El valor introducido es incorrecto.")]
        public string LastName { get; set; }

        [Required(ErrorMessage="Este campo es obligatorio.")]
        [DataType(DataType.Date, ErrorMessage = "El valor introducido es incorrecto.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage="El email introducido es incorrecto.")]
        public string Email { get; set; }

        public string Street { get; set; }

        public int IntNumber { get; set; }

        public int ExNumber { get; set; }

        public string Colony { get; set; }

        public int Country { get; set; }

        public int State { get; set; }

        public string City { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage="El teléfono introducido es incorrecto.")]
        public string Phone { get; set; }

        [Range(0, 50000, ErrorMessage="El campo {0} no debe exceder los {2}.")]
        [DataType(DataType.Currency, ErrorMessage = "El valor introducido es incorrecto.")]
        [RestrictedAmount] // Business Rules
        public decimal Amount { get; set; }

        [Required(ErrorMessage="Este campo es obligatorio.")]
        [Range(0, 2, ErrorMessage="Este campo es obligatorio.")]
        public Risk Risk { get; set; }

        [Required(ErrorMessage="Este campo es obligatorio.")]
        [StringLength(250, ErrorMessage = "La longitud del campo {0} debe ser entre {2} y {1} caracteres.", MinimumLength = 50)]
        public string Observations { get; set; }
    }
}