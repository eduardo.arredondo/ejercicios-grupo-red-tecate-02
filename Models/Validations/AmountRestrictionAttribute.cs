using System;
using System.ComponentModel.DataAnnotations;
using DSIELABGRMVCTest.Models.Enums;

// Business Rules Validation Namespace
namespace DSIELABGRMVCTest.Models.Validations
{
    public class RestrictedAmount: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var formData = (FormDataViewModel)validationContext.ObjectInstance;
            var invalid = false;
            int age = new DateTime(System.DateTime.Now.Subtract(formData.BirthDate).Ticks).Year - 1;
            
            if (age > 65 && (decimal)value > 10000) { // Si es mayor de 65 años la cantidad no puede ser mayor 10,000
                invalid = true;
                this.ErrorMessage = "Si la edad es mayor de 65 años el monto no puede exceder los 10000";
            } else if (formData.Risk == Risk.High && (decimal)value > 5000) { // Si el riesgo es alto no mayor de 5,000
                invalid = true;
                this.ErrorMessage = "Si el riesgo es alto el monto no debe exceder los 5000";
            } else if (formData.Risk == Risk.Medium && age > 50 && (decimal)value > 7500) { // Si es medio y mayor a 50, no mayor a 7,500
                invalid = true;
                this.ErrorMessage = "Si la edad es mayor de 50 años y el riesgo es medio el monto no debe exceder los 7500";
            } else if ((decimal)value > 50000) { // Ninguna cantidad debe pasar de 50,000
                invalid = true;
                this.ErrorMessage = "El monto no debe exceder los 50000";
            } else if ((decimal)value == 0) { // Campo obligatorio
                invalid = true;
                this.ErrorMessage = "Este campo es obligatorio.";
            }

            return !invalid ? ValidationResult.Success : new ValidationResult(this.ErrorMessage);
        }
    }  
}