﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DSIELABGRMVCTest.Models;

namespace DSIELABGRMVCTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            FormDataViewModel formData = new FormDataViewModel();
            return View(formData);
        }

        [HttpPost]
        public IActionResult Index(
            [Bind("Name, LastName, BirthDate, Street, IntNumber, ExNumber, Colony, Country, State, City, Phone, Email, Amount, Risk, Observations")] FormDataViewModel formData
        )
        {
            if (ModelState.IsValid) {
                ViewData["message"] = "Datos validados correctamete";
            }
            return View(formData);            
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
